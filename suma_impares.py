n1 = int(input("Dame un número entero no negativo: "))

if n1 < 0:
    print("El número ingresado no es no negativo.")
else:
    n2 = int(input("Dame otro: "))
    if n2 < 0:
        print("El segundo número ingresado no es no negativo.")
    else:
        suma = 0
        min = min(n1, n2)
        max = max(n1, n2)

        for n in range(min, max + 1):
            if n % 2 != 0:
                suma += n
                
        print("La suma de los números impares entre",min,"y",max,"es:",suma)